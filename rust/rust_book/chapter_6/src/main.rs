/*
Enum enumerate all the possible values of something. Enum values can only be
one of the variants. We can specify the type and the amount of data that each
variant will hold. enums can have methods.
*/
#[derive(Debug)]
enum IpAddr {
    V4 (u8, u8, u8, u8), 
    V6 (String),
}
impl IpAddr {
    fn show_address(&self){
        println!("{:?}", &self);
    }
}



fn main() {
    let home = IpAddr::V4(127, 0, 0, 1);
    let loopback = IpAddr::V6(String::from("::1"));
    home.show_address();
    loopback.show_address();

    /*
    This will break because "i8" and "Option<i8>" are differente types.
    Option<T> library is explained in chapter 6 of the Rust Book, please refer
    to it for wider explanation.

    enum Option<T> {
        Some(T),
        None,
    }

    <T> means that Some variant can hold one piece of data at a time.
    If we use None we need to tell Rust what type of Option<T> we have.
    let absent_number: Option<i32>= None;
    
    You need to convert Option<i8> to i8 before performing an operation with it.
    
    */
    let x: i8 = 5;
    let y: Option<i8>= Some(5);

    /*
    match arms can bind to the parts of the values that match the pattern. This
    is how we can extract values out of enum variants.
    Matching with Option<T>
    Some(5) matches with Some(i), the i binds to the value contained in Some, so i
    takes the value 5. See lines 64 and 65.
    */
    fn plus_one(x: Option<i32>) -> Option<i32> {
        match x {
            None => None,
            Some(i) => Some(i+1)
        }
    }
    
    fn plus_one_v2(x: Option<i32>) -> i32 {
        match x {
            None => 0,
            Some(i) => i+1
        }
    }
    let five = Some (5);
    let six = plus_one(five);

    /*
    Matches in Rust are exhaustive, we must exhaust every last possibility in
    order to be valid. The "_" pattern will match any value. By putting it
    after our other arms the "_" will match all the possible cases that aren't
    specified before it. The () is the unit value, so nothing will happen in
    the "_" case.
    */
    let some_u8_value = 0u8;
    match some_u8_value{
        1=> println!("one"),
        2=> println!("Two"),
        _ => (),
    }

    #[derive(Debug)]
    enum State {
        Alabama,
        Alaska,
    }
    enum Coin {
        Penny,
        Nickel,
        Quarter(State)
    }
    /*
    Control flow "if let". This let's handle values that match one pattern
    while ignoring the rest. Using "if let" means less typing, less identation
    and less boilerplate code, however, you lose the exaustive checking that
    match offers.
    */
    let mut count = 0;
    let coin = Coin::Quarter(State::Alaska);
    if let Coin::Quarter(state) = coin {
        println!("Sate Quarter from {:?}!", state);
    } else {
        count += 1; 
    }
}
