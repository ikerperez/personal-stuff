/* 
Collections: contain multiple values
    Vectors: store variable number of values next to each other, they must be
    values of the same type. USE: List of Items
    string: collection of characters
    hash map: associate a value with a key.
*/



fn main() {

    // VECTORS
    let mut v1: Vec<i32> = Vec::new(); // Create a mmutable vector
    v1.push(5); // Add elements to a vector
    v1.push(6);
    v1.push(7);

    let v2 = vec![1, 2 , 3, 4, 5];

    /*
    The ways to access an element from a vector are by using either & and []
    (giving as a reference) or using the "get" method (giving us Option<&T>).

    The [] method will cause the program to panic if references a non existing
    element.
    The get method will return "None" withput panicking.
    */
    let third: &i32 = &v2[2]; // One way of access a value in a vector
    match v2.get(2) {         // Another eay to access a value in a vector
        Some(third) => println! ("This elements id {}", third),
        None => println!("There is no third element"),
    }

    //Iterating over elements in a vector
    for i in &v2 {
        println!("{}", i);
    }
    /* 
    Iterate over mutable references to each element in a mutable vector in
    order to make chanes to the elements.
    We have to use the deference operator (*) to get the value in i (like C).
    */
    for i in &mut v1 {
        *i += 50;
    }

    /*
    Storing multiple types

    We can use an enum to store multiple types in a vector. Rust needs to know
    how what types will be at compile time, an enum lets youdo that. If you know
    the exaustive types the program will get at run time you will need to use a
    trait.
    */
    enum SpreadsheetCell {
        Int(i32),
        Float(f64),
        Text(String),
    }
    let row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Text(String::from("blue")),
        SpreadsheetCell::Float(10.12),
    ];

    /*
    STRINGS

    Strings are implemented as a collection of bytes
    * string slice "str" are references to some UTF-8 encoded string data
    stored elsewhere. 
    * string literals are string slices stored in the program's binary.
    * the String typeis a growadable, mutable, owned UTF-8 encoded string type
    stored in the heap.
    
    Strings can store text in diferent encodings or be represented in memory in
    different ways.
    */

    let mut s= String::new(); //Creates an empty string in s.
    let data = "initial string";
    let s2 = data.to_string(); //Creates a string containg the text in data.
    /*
    Creates a string from a string literal, no need to hold the string in a
    variable first.
    */
    let s3 = String::from("initial string");

    /*
    Updating a string

    Use "push_str" to append a string slice.
    Use "push" append a single character.
    */
    s.push_str("hello world");
    s.push('!');

    /*
    String Concatenation

    "+" Operator: Uses the "add" method. fn add(self, s: &str) -> String {
    * format! macro: Works like println but instead of printing to the screen
    retuns a String with the contents. Doesn't take ownershiop.
    */

    let s4= s3 + & s; //s3 has been moved here and can not be longer be used because it takes ownership.
    let s5= format!("{}-{}-{}", s, s2, s3);

    /*
    Rust string don't support indexing. For more information go to the rust
    book, chapter 8 page 141. Instead string slices are used.
    */
    let s6 = &s[0..3]; //s6 will contain the first 3 bystes of s.

    /*
    Iteraing over strings

    For unicode scalar values use "chars" method.
    The "byte" method retirns eahc raw byte.
    */
    for c in "नमस्ते".chars() {
        println!("{}", c);
    }
    for b in "नमस्ते".bytes() {
        println!("{}", b);
    }

    /*
    HASH MAPS

    The type HashMap<K, V> stores a mapping of keys of type K to values of type
    V. Hash maps are useful when you want to look up data not by using an index
    but by using a key that can be of any type.

    Data is stored in the heap. Hash maps are homogeneous: all of the keys must have the same type, and all of the values must have the same type.
    */

    //You can create an empty hash map with new and add elements with insert.
    use std::collections::HashMap;

    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    /*
    Another way of constructing a hash map is by using iterators and the
    collect method on a vector of tuples.
    The collect method gathers data into a number of collection types, including HashMap.
    Use the zip method to create a vector of tuples where “Blue” is paired with 10, and so forth. 
    */
    let teams = vec![String::from("Blue"), String::from("Yellow")];
    let initial_scores = vec![10, 50];
    /*
    The type annotation HashMap<_, _> is needed here because it’s possible to collect into many different data structures
    */
    let mut scores: HashMap<_, _> =
        teams.into_iter().zip(initial_scores.into_iter()).collect();

    /*
    For types that implement the Copy trait, like i32, the values are copied into the hash map. For owned values like String, the values will be moved and the hash map will be the owner of those values.
    */

    //Accessing values in hash map
    //Providing the key to the get method
    let value = scores.get("Blue");

    //Iterate using for loop
    for(key, value) in &scores {
        println!("{}:{}", key, value);
    }

    /*
    Updating the hash map. 
    Each key can only have one value. If we insert a key and value into hash
    map and then insert that same key with a different value,the value
    associated with the key will be replaced.

    To only introduce a value if the key has not an associated value you can
    use "entry". "Entry" will return a enum called "Entry that represents the
    value that might or might not exist."
    */
    let mut scores2 = HashMap::new();
    scores2.insert(String::from("Blue"), 10);

    /*
    The or_insert method on Entry is defined to return a mutable reference to the value for the corresponding Entry key if that key exists, and if not, inserts the parameter as the new value for this key and returns a mutable reference to the new value.
    */
    scores2.entry(String::from("Yellow")).or_insert(50);
    scores2.entry(String::from("Blue")).or_insert(50);

    println!("{:?}", scores2);

    //Update a vlue based on the old value
    /*
    Here we store that mutable reference in the count variable, so in order to assign to that value, we must first dereference count using the asterisk (*). The mutable reference goes out of scope at the end of the for loop, so all of these changes are safe and allowed by the borrowing rules.

    */
    let text = "hello world wonderful world";

    let mut map = HashMap::new();

    for word in text.split_whitespace() {
        let count = map.entry(word).or_insert(0);
        *count += 1;
    }

    println!("{:?}", map);


}
