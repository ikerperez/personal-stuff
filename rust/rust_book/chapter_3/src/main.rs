use std::io;

fn temperature_conversion() -> f64 {
    println!("Give me a temperature to be converted from Celsius to Farenheit");
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Input Error");
    let input: f64 = match input.trim().parse(){
        Ok(num) => num,
        Err(_) => {
            println!("A number was not given");
            panic!();
        },
    };
    input*1.8 + 32.0         
}

fn fibonacci() -> u32{
    println!("Which number in the series you wanna see?");
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Give me a natural number");
    let input: u32 = match input.trim().parse(){
        Ok(num) => num,
        Err(_) => {
            // If panic is reacher the program crashes
            panic!("A number was not given");
        },
    };
    let mut fibo = 1;
    let mut fibo_old = 0;
    if input == 1 {
        fibo
    } else {
        //Rust number ranges equal to " < input", to have a "<= input" use "..=input".
        for _number in 2..=input{
            fibo += fibo_old;
            fibo_old = fibo - fibo_old;
        }
        fibo
    }
}

fn christmas_carol() {
    let presents =["A partridge in a pear tree\n", "Two turtle doves And ",
    "Three French hens, ", "Four calling birds, ", "Five gold rings, ",
    "Six geese a laying, ", "Seven swans a swimming, ", "Eight maids a milking, ",
    "Nine drummers drumming, ", "Ten pipers piping, ", "Eleven ladies dancing, ", 
    "Twelve Lords a leaping, "]; 
    for day in 0..12 {
        println!("On the {} day of Christmas my true love sent me", day);
        //.rev() make the loop iterate in the reverse order
        for present in (0..=day).rev() {
            print!("{}", presents[present]);
        }
    }
}

fn main() {

    println!("Which function you want to execute:");
    println!("Type: 'Temperature' for Temperature Conversion");
    println!("Type 'Fibonacci' for nth Fibonacci number");
    println!("Type 'Carol' for Christmas Carol");
    let mut decision = String::new();
    io::stdin().read_line(&mut decision).expect("ERROR:");
    match decision.trim(){
        // The | allows Pattern alternatives.
        "Temperature" | "temperature" => {
            println!("The temperature is {}", temperature_conversion());
        },
        "Fibonacci" | "fibonacci" => {
            println!("The number is {}", fibonacci());
        },
        "Carol" | "carol" => {
            christmas_carol();
        }
        &_ => {
            panic!("QUE TE DICHO?!")
        }
    };
}
