fn takes_ownership(some_string: String) {
    println!("{}", some_string);
} /*
  some_string goes out of scope and `drop` is called. The backing memory is
  freed.
  */

fn makes_copy(some_integer: i32) {
    println!{"{}", some_integer}
} //some_integer goes out of scope.

//& is a borrow of a value (The other side of a reference)
fn calculate_lenght(s: &String) -> usize {
    s.len()
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}

/*
Slices are a continuos sequence of elements in a collection, strings are slices.
The paramentes of "first_word" have been decided to be string slices because
will allow to use Strings and string literals in the same function, if we
would have provided a "String" string literals wouldn't be accepted. See
fn "calculate_lenght".
*/
fn first_word(s: &str) -> &str {
    /*
    In rust strings are not quite arrays of characters, they are slices.
    Therefore, you can't just loop over the string like in C, you need to use
    an iterator. First you need to convert the string to an array of bytes.
    */
    let bytes = s.as_bytes();

    /*
    Iter() is a method that returns each element in a collection and
    enumerate() wraps the result of iter and returns a tuple containing the
    index of the item and a reference to the item.
    */
    for (i, &item) in bytes.iter().enumerate() {
        // Search for the byte that represents a space in the byte literal syntax.
        if item == b' ' {
            /*
            If we find a space, return a string slice from the starting of the
            slice and the index of the space as ending indice.
            */
            return &s[..i];
        }
    }
    // If space is not found, return all the string.
    &s[..]
}

fn main() {
    /*
    This is a string literal. It's inmutable and is stored in the stack.
    It's need to be a known size at compile time.
    */
     let _s_literal = "Hello";
     /*
     This is a "String" type, is allocated in the heap, can be defined as
     mutable and is able to store an amount of text that is unkown at
     compile time.
     */
    let mut s =  String::from("hello");
    s.push_str(", world!"); //push_str() appends a literal to String.

     /*
     s value moves into the function and so is not longer valid. This is
     because the string is stored in the heap, and instead of cloning it
     the ownserhsip is transfered. See book page: 64
     */
    takes_ownership(s);

    /*
    x would move into the function, but x is a i32, therefore a `copy` is done,
    leaving x untouched.
    */
    let x = 5;
    makes_copy(x);

    //REFERENCES AND BORROWING
    let s1 = String::from("hello");
    /*
    & refers to a value without taking ownership of it. Because it doesn't own
    it, the value won't be dropped when the reference goes out of scope.
    References are inmutable by default.
    */
    let len = calculate_lenght(&s1);
    println!("The lenght of '{}' is {}.", s1, len);

    /*
    To make a mutable reference, we had to create a mutable reference with
    'mut s' and accept a mutable reference with 'some_string: &mut String'.
    YOU CAN ONLY HAVE ONE MUTABLE REFERENCE TO A PARTICULAR PIECE OF DATA
    IN A PARTICULAR SCOPE. But you can use curly brackets to create a new
    scope, allowing multiple mutable references. It is not possible to
    have mutable reference while we have an inmutable one.
    */
    let mut s2 = String::from("Hello");
    change(&mut s2);
    {
        change(&mut s2);
    }

   let word = first_word(&s2);
   println!("The first word is {}", word);
   
}
/*
x goes out of scope, then s is meant to go out of scope, buut because ownership
was changed and the memory allocated to s was freed in `takes_ownership` nothing
special happens.
*/
