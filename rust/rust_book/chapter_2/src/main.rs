use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn main() {
    let secret_number = rand::thread_rng().gen_range(0, 100);
    
    loop {
            println!("Give me a number");
            let mut guess = String::new();
            io::stdin().read_line(&mut guess)
                    .expect("Input Error");
            /*
            Create a new varible with the same "guess" name to shadow the old
            variable. Shadowing lets you reuse the variable name rather than
            forcing you to create two unique variables. Used in value type
            convertions.
            
            trim() eliminates any whitespaces in strings, since the user when
            pressing ENTER will introduce "\n" we need to get rid of it.

            parse() will convert the given value to the type of the target
            variable. The type of the variable is defined by ": u32".

            parse() will return a "Result" type than can be matched to
            perform an action depending of the value.

            */
            let guess: u32 = match guess.trim().parse() {
                Ok(num) => num,
                Err(_) => {
                    println!("I Only accept numbers");
                    continue;
                },
            };

            /*
             Use match to decide what to do next based in which variant of Ordering was
             returned from cmp.
            */
             match guess.cmp(&secret_number){
                /*
                This are arms: Consists of a pattern and the code that should be run in
                the value given to the beginning of the match expression fits the
                arms's pattern.
                */
                Ordering::Less => println! ("Too Small!"),
                Ordering::Equal => {
                    println! ("Nice!");
                    break;
                },
                Ordering::Greater => println! ("Too Big!"),
            }
    }
}
