//OPTION 1
fn area_basic(widht: u32, height: u32) -> u32 {
    widht*height
}

//OPTION 2
fn area_tuple (dimensions: (u32, u32)) -> u32 {
    dimensions.0 * dimensions.1
}

/*
OPTION 3: Structure files should be snake_case, however for the porpuse of
this example and to avoid not meaningful field names, I decided to copy the
field naem in UPPER case. Structs don't have a implementation for Display, the
formating used by println!(), therefore if we want to print the contemts of a
structure we need to implement the Debug functionality to the structure and 
tell the compiler that we want to use the debug formating.
The derive annotation allows us do that implement the Debug functionality to
the structure.
*/
#[derive(Debug)]
struct Rectangle {
    WIDHT: u32,
    HEIGHT: u32
}
/*
We borrow the structure instead of taing ownership, so "main" mantains
ownership and can continue using rect1.
*/
fn area_structure(rectangle: &Rectangle) -> u32 {
    rectangle.WIDHT * rectangle.HEIGHT
}

/*
OPTION 4: To define a function withim the Rectangle context, we start an impl
block.
*/
impl Rectangle {
    /*
    We use &self in the signature instead of "rectangle: &Rectangle" because
    Rust knows the type of "self" is "Rectangle" due to this method being
    called inside the "impl" Rectangle context. Methods can take ownership of
    self, borrow a self inmmutably or borrow self mutably.
    */
    fn area(&self) -> u32 {
        self.WIDHT * self.HEIGHT
    }
    //Declaration of a second parameter in a method.
    fn can_hold(&self, rectangle: &Rectangle) -> bool {
        /*
        Because we specified that this method will return a "bool" we don't need
        to explicitly add an "if else" condition and "true, false" expressions.
        Rust handles that by deciding if the expression below resolves to "true"
        or "false".
        */
        self.HEIGHT > rectangle.HEIGHT && self.WIDHT > rectangle.WIDHT
    }

    /*
    This is not a method, this is an "Associated Function", this are functions
    within "impl" blocks that don't take "self" as a parameter, they don't have
    an instance of the struct to work with. They are usually used for
    constructors that will return a new instance of the struct. We call them
    with "::". 
    */
    fn  square(size: u32) -> Rectangle {
        Rectangle {WIDHT: size, HEIGHT: size}
    }
}

fn test_fitness(rect1: &Rectangle, rect2: &Rectangle) {
    if rect1.can_hold(&rect2) {
        println!("The first rectangle can gold the second one");
    } else {
        println!("The first rectangle can't gold the second one");
    }
}

fn main(){
    let widht = 20;
    let height = 10;
    let dimensions = (widht,height);
    /*
    Have been avoided to use "field init shothand" syntax for future clarity.
    The same could have been achieved with:
    struct Rectangle {
        widht: u32,
        height: u32
    }
    let rect1 = Rectangle {
        widht,
        height,
    };
    */
    let rect1 = Rectangle {
        WIDHT: widht,
        HEIGHT: height,
    };
    let rect2 = Rectangle {
        WIDHT: 10,
        HEIGHT: 5,
    };
    let rect3 = Rectangle {
        WIDHT: 30,
        HEIGHT: 10,
    };
    //OPTION 1
    println!("V1: The are of the rectangle is {}", area_basic(widht,height));
    
    /*
    OPTION 2: Use tuples. Tupples let us add a bit of structure and instead of
    passing two arguments to the are calculator function, we just pass one. But is less clear becuase tuples
    don't name their elements.
    */
    println!("V2: The are of the rectangle is {}", area_tuple(dimensions));
    
    /*
    OPTION 3: Use structures. Structure creates a relation between height and
    width, allows just to pass one argument to the function and gives
    descriptive names to values.  
    */
    println!("V3: The are of the rectangle is {}", area_structure(&rect1));
    // :? Tells the compiler to use the debug formating.
    println!("V3-1: {:?}", rect1);
    // :#? Debug formating for large structs.
    println!("V3-2: {:#?}", rect1);

    /*
    OPTION 4: Structures and methods. Methods "functions" defined within the
    context of structures, enums or trait objects, their first parameters is
    always "self". which represents the instance of the struct the method is
    being called on. The  method syntax foes after an instance: we add a dot
    followed by the method name, parenthesis and any arguments. Benefits of
    methods: 
      - Using method syntax.
      - Not having to repeat the type of "self" in every method signature.
      - Organization: All things an instance can do in one "impl" block.
    */
    println!("V4: The are of the rectangle is {}", rect1.area());

    test_fitness(&rect1, &rect2);
    test_fitness(&rect1, &rect3);

    let square = Rectangle::square(10);
    println!("V4-1: The are of the square is {}", square.area());
}
