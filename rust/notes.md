# Tools
## Rustop
Manages rust toolchains.

```rustup show```: Give information about environment

```rustup component add```: add components

rustformat -->

## Cargo
package manager, it builds and runs the code, install packages,
manage dependencies, version control.  each program as  independent cargo project

```cargo clippy``` linking tool

# Documentation

```rustuip --std```: standard library documentation

```rustop doc```: General documentation

```rustup doc --reference```: rust reference documentation

# Create and run a program
```cargo new --bin``` to create a program.

```cargo run``` to compile+run a program. By adding ```-- ARGUMENTS_FOR_PROGRAM```
can be given input values to the program.

## ERRORS
```Panic``` is a controlled explosion, it would cause undesired behaviour

```cargo``` provides command s to understand ```ignore_errors```

# General
```&``` is pronounced in rust ```borrow```.

 slice in rust is a fundamental type, like an array in C.

## Functions
 inside a function specify n: to select a number of elements.

 fn hello (n: )

## Data types:
 u ->
 i ->
 size -> size of the elements. Dependant f the OS

 uri to specify number of bits independently f the OS

 _ means any type of value

parse::<type> tells which type parse to

() -> Unit, like void in C.

## Control loops

 ```loop```  is infinite loop, only stopped by a break statement.

## Expressions
in rust the values of the expression/function  is the value of the last executed expression

statements are expressions

matches is a expression

## Iterators
Double ended iterator --> research

iterators:
 next
 skip

## Formatting
parse --> convert string into another type

## Modules
fs modules let you read from the system, "read_to_string"

## Error handling
? is for error handling

Check for bindings, same name for variable but different item

## Struct
Similar to C structure. By default the fields of a struct re private, unless you prepend "pub" into them. Usually the best is to keep the data private and have accesses into them. 
You can't print a privae member of a struct like "println!("Mi name is {}", person.name);" you need "println!("Mi name is {}", person.name());"

struct Mystruct {
    field1: SomeType,
    field2: SomeType
}

struct person {
    name: String,
    age: usize,
}

==============================
impl person{
    pub fn new(name: &str, age: usize) -> Person {
        Person {
            name: name.to_owned(),
            age,
        }
    }
}
## Enums

enum Myenum {
    UnitKind,
    TupleKind(SomeType, SomeOtherType),
    StructKind { field1: SomeType, field2: SomeOtherType}
}



trait collection of behaviour, you can use something in a [articular way.]


## Derive attribute

#[derive(Debug)]

# Libraries
cargo.lock
cargo.tml

workspace are directories that contain mmultiple crates

crates.io: repository of crates tha have been published

# Comand line arguments

structopt
clap
