fn say_hello(name: &str) {
    println!("Hello {}!",  name);
}

fn bottle (n: usize) -> &'static str {
    if n == 1 {
        "bottle"
    } else {
        "bottles"
    }
}

fn verse (n: usize) {
    println! ("{} green {} spookily floatin in the midair", n, bottle(n));
    println! ("{} green {} spookily floatin in the midair", n-1, bottle(n-1));
}

fn main () {

    say_hello("iker");
    let num_verses: usize = match std::env::args().skip(1).next() {
        Some(val) => val.parse().unwrap(),
        None => unimplemented!(),
    };
    for round in (1..=10).rev() {
        verse(round);
    }
}
