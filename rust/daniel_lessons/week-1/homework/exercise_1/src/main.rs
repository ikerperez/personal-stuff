use env::args;
use std::env;
use std::fs;
use std::io;
use std::io::BufReader;
use std::iter;
use std::path;
use std::result::Result;
use std::string;

/*
Error handling: Result<T,E> is a type that holds two possible values Success or
Failure (is it like a Boolean?). On it you specify the value that "Result" should
have when there is success (T) or when there is failure (E). "?" can be appended to
statements to actually return the value stored in "Return".
Box(x): Prepare(allocate) memory in the heap and store x on it.
() or unit: This is a type that has one value and it's used when there isn't
any other meaningful value returded. In human words, when a statement/function
returns some weird/non-usual/awkward value this helps working with it.
dyn:
    Methods are functions attached to objects. These methods have access to the
    data of the object and its other methods via the self keyword. In human words:
    Methods define a set of characteristics/habilities of a specific object (Python)
    Traits: A trait is a collection of methods defined for an unknown type: Self.
    For example we could create the trait "Animal".
    trait object: A trait object is an opaque value of another type that implements
    a set of traits.
    dyn lets you create a trait object.
Error: is a trait representing the basic expectations for error values.
Box<dyn std::error::Error>: Is allocation in the heap a trait object that contains
the values from the Error trait. There are two pointers that point to the error and
the error structture behind it which are allocated in the heap.

*/
fn main() -> Result<(), Box<dyn std::error::Error>> {
    let file = env::args().skip(1).next().ok_or("No file")?;
    let content = std::fs::read_to_string(file)?;

    println!("Content: {}", content);

    Ok(())
    /*
    After the ok(()) you don't need the ";" because that terminates a statement and it would
    make te function return "()" because there is nothing after the "ok(())".
    */
}

// Option 1
/*fn main ()-> Result<(), Box<dyn std::error::Error>>{
    let filename = args().skip(1).next().ok_or("No file")?;

    let fh = File::open(filename);

    let reader = BufReader::new(fh);

    let mut sum = 0;

    for line in reader.lines() {
        let line = line?;
        let val: i128 = line.parse()?;
        sum += val;
    }
    println!("values {}", sum);
}
*/
// Option 2
/*
fn option2 ()-> Result<(), Box<dyn std::error::Error>>{
    let filename = args().nth(1).ok_or("No file")?;

    let content = std::fs::read_to_string(filename)?;

    let sum: Result<i128, _> = content.lines().map(|line| line.parse::<i128>()).sum();
    println!("values {}", sum?);
    Ok(())
}
*/
