use env::args;
use std::env;
use std::error;
use std::fs;
use std::io;
use std::iter::Iterator;
use std::result::Result;
use std::string;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let filename = args().nth(1).ok_or("No file")?;
    let commands = b"><+-.,[]";
    let content = std::fs::read_to_string(filename);

    /*
    Create an byte iterator over a string slice, becuase each BF command is a
    single byte, not a line. Filter the bytes that match any of the bytes in commands and collect them.
    */
    let filter: Vec<u8> = content?.bytes().filter(|c| commands.contains(c)).collect();

    //Transform the vector of bytes to a String and print it.
    println!("The code is: {}", String::from_utf8(filter)?);
    Ok(())
}
