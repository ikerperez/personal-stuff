use std::convert::TryFrom;

enum InputLine {
    NameOnly(String)
    NameandNumber(String, usize)
}

impl TryFrom<AsRef<&str> for InoutLine{
    type Error = Box<dyn std::erro::Error>;

    let mut chunks = input.split{':');
    let name: = if let Some (chunk)= chunk.next() {
        chunk.to_string()
    } else {
        return Err ("Empy input?".into());
    };

    let num: usie = match chunk.nxt() {
        None -> return OK (InputLine::NameOnly(name)),
        Some(v) -> v.parse()?,
    };

    if chunk.nect().is_some(){
        Err("Bad input, more than one colon",into())
    } else {
        OK(InoutLine::NameandNumber(name,num))
    }

    

    /* Alternative
    fn try_from(input: &string) -> Result<Self, self::Error> {
        if input.split(':').count() > 2{

        } else if input.split(':').count = 1 {

        }else 
    }*/
}

fn main() {

}