use std::collections::HashMap;
use std::convert::TryFrom;
use std::env;
use std::fmt;
use std::fs;
use std::path::Path;

// Create the GenError type for shorteness in the future.
type GenError = Box<dyn std::error::Error>;

#[derive(Debug)]
enum InputLine {
    NameNumber(String, usize),
    Name(String),
}

//Define a trait for Inputline for type conversion
impl TryFrom<&String> for InputLine {
    //Define the error type to be used.
    type Error = GenError;

    /*
    Create a method function
    */
    fn try_from(input: &String) -> Result<Self, Self::Error> {
        let mut chunks = input.split(':');
        /*
        Use the "if let" expression to avoid "match", it makes it simpler.
        If there is any data in the chunk, convert it to string and save it. If not, report an error.
        */
        let name = if let Some(chunk) = chunks.next() {
            chunk.to_string()
        } else {
            /*
            Return the error type defined in line 12. into() converts the
            tring into the defined error type.
            */
            return Err("Empy data".into());
        };

        /*
        Check if there is more data in the string, if there is, it is number, if there is not, return the Name variation of InputLine
        */
        let number: usize = match chunks.next() {
            Some(chunk) => chunk.parse()?,
            None => return Ok(InputLine::Name(name)),
        };

        //Check if there is more data in the array.
        if chunks.next().is_some() {
            //Return the error type defined in line 12
            Err("Data excess".into())
        } else {
            //return the NameNumber variation of InputLine
            Ok(InputLine::NameNumber(name, number))
        }
    }
}

//Default trait is needed to provide default values to the any Scores type structure
#[derive(Default, Debug)]
struct Scores {
    num_scores: usize,
    sum_score: usize,
    missed_tests: usize,
}

//Traits to make calculations based in the tests of the student
impl Scores {
    fn add_scores(&mut self, score: usize) {
        self.sum_score += score;
        self.num_scores += 1;
    }

    fn missed_test(&mut self) {
        self.missed_tests += 1;
    }
}

/*
Implementation of Display Trait for Scores, this will make possible to use println!
*/
impl fmt::Display for Scores {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> fmt::Result {
        //This is a Closure, makes possible to choose the value of test_n in base of n's value.
        let test_n = |n| if n == 1 { "test" } else { "tests" };
        write!(
            f,
            "took {} {}, with a total score of {}. They missed {} {}.",
            self.num_scores,
            test_n(self.missed_tests),
            self.sum_score,
            self.missed_tests,
            test_n(self.missed_tests)
        )
    }
}

fn read_values<P: AsRef<Path>>(path: P) -> Result<Vec<InputLine>, GenError> {
    let content: String = fs::read_to_string(path)?;
    //This line performs the same actions that the code commented below.
    content
        .lines()
        .map(|x| InputLine::try_from(&x.to_string()))
        .collect()
    //let mut data: Vec<InputLine> = Vec::new();
    //let mut x: InputLine;
    //for line in content.lines() {
    //    x = InputLine::try_from(&line.to_string())?;
    //    data.push(x);
    //}
    //Ok(data)
}

fn main() -> Result<(), GenError> {
    let file = env::args().nth(1).ok_or("need a path")?;
    let scores_vector = read_values(file);
    // Create a new hash map
    let mut students: HashMap<String, Scores> = HashMap::new();

    // Loop over each value in scores_vector
    for student in scores_vector? {
        /*
        Save values of the student scores depending of the enum variant.
        Number needs to be wrapped with "Option" type enum because in case
        of the score being a InputLine::Name, there is not going to be
        "number" value, therefore we need to use a "None" to fill the
        corresponding tupple space.
        */
        let (name, number): (String, Option<usize>) = match student {
            InputLine::Name(name) => (name, None),
            InputLine::NameNumber(name, number) => (name, Some(number)),
        };

        /*
        .entry() will take a mutable borrow from  the "students" hashkey, making
        possible to performs actions over the value while the value is still
        stored in "students", this mutable borrow reference will be stored in
        "entry" variable. .or_default() will make sure that if an specific entry
        doesn't exist, it is created by the default values, this is possible thanks
        to line 62.
        */
        let entry = students.entry(name).or_default();
        if let Some(number) = number {
            entry.add_scores(number);
        } else {
            entry.missed_test();
        };
    }

    for (name, scores) in students {
        println!("{} took {}", name, scores);
    }

    Ok(())
}
