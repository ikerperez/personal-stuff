# Data Streams
## Flow Control (stdin, stdout, stderr in the terminal)
	* You can give a file to a program that is expecting standard input (keyboard usually) by doing ./program < file.txt.
	* You can send the output of a file that goes to standard output (screen usually) by doing ./program > output.txt
	* Mix both: ./program < file.txt > output.txt
	* To take the output of a program and send it to another, same time stdin from file and stdou to another file. (./program1 | ./program2) < input.txt > output.txt

## Manage stdin, stdout stderr from C
#### printf (), scanf() and fgets()

	* Adding a number between % and d (for example %5d):
		* printf(): Will increase the printed numbers/elements until that number. The numbers  will be
		  justified to the right by the number introduced. It basically says that the number will be at
		  least as wide as the number. With floats, you can do the same and decide how many decimals are
		  you interested in: %3.2f so 2 decimals
		* scanf(): Will put a limit on the number of characters that scanf() will read. Not limiting the amount
		  of characters to be read can create a buffer overflow if the number introduced is bigger that the
		  lenght of the array to be saved in. For example: char food [2];  scanf ("%s", a); and introduce "coconut".
		  The reason is because scanf() writes data way beyond the end of the space allocated to the food array.
		  You need to substract 1 from the lengh to leave space for the sentinel character.
	* **fgets** is an alternative to scanf but unlike the scanf() function, the fgets() function must be given a maximum length.
	  fgets(food, sizeof(food), stdin). sizeof handles automatically the sentinel character. If you are passing a pointer,
	  remember that sizeof will not return the length of the array but the size of the pointer, so you will have to give the
	  length explicitly.

#### getchar() and putchar()
* c = getchar() read the next character from de data stream and stores it in c. Usually the keyboard.
* putchar(c) writes the content of c like character

#### fprintf and fscanf
The fprintf() and fscanf functions allow you to choose where you want to send/read text to.
You can tell fprintf() to send text to stdout, stderr or a file.
	* fprintf(stdout, "I like Turtles!"); --> Sending data to standard output (screen usually)
	* fprinft(stderr, "Error=2"); -> Sending data to standard error

#### sprintf
Instead of printing on console, it store output on char buffer which are specified in sprintf.
sprintf allows you to store strings in variables.
## Create your own data streams
**fopen()** creates a new data stream, represented by a pointer to a file.

```
FILE *in_file = fopen("filename.txt", "mode"); --> being mode options to be: r(read), w(write) and a(append).
```

Once the data stream is created  you can print to it usinf fprintf -> fprintf (out_file, "Hello world");
When you are finished with a data stream you need to close it.

```
fclose(in_file);
```

# Quoting
	* " " For strings
	* ' ' For arrays.

# Pointers
	* When printing an address %p is used to format it.
	* The & operator takes a piece of data and tells you where it’s stored.
	* The * operator takes an address and tells you what’s stored there. It's used for declaring pointer variables too
	* On 32-bit operating systems, a pointer takes 4 bytes of memory and on 64-bit operating systems, a pointer takes 8 bytes.
	* A void pointer can store the address of any kind of data, but you always need to cast it to something more specific before you can use it.

```
const u16 *name -> Constant value
u16 *const name -> constant address
```

# Arrays and Strings
	* If an array is declared like char array [] you need to initialize it at the moment of declaring it.
	* If an array is declared as part of an argument of a function, the array is a pointer. void foo (char cards []) cards is a pointer and is equivalent to
```
void foo (char *cards)
```
	* Characters strings must have the same size as the number of characters to be stored + 1. Because the sentinel character (or null)
	  must be stored too. char val [3] = "no"; will store in val [n , o, \o].
	*  **String literals**: When storing a string with "" it will be stored as a string literal, they are easier to type and they are constant. That's because
	  they will be stored in the a "constant" memory block, so they're read-only. Because of that, a variable that points to a string literal can’t
	  be used to change the contents of the string	`char *cards = "JQK";` Can't be modified because this is a pointer the address of the constant string
	  If you create a copy of the string in an area of memory that’s not read-only, there won’t be a problem: `char cards[] = "JQK";` Can be modified because
	  it is a copy of the constant string in memory's stack zone (r/w), it's a proper array.
	  A way to avoid the problem is to make sure you use the const
	  keyword: `const char *s = "some string";` and the compiler will warn you if sees some code that tries to modify the string.
	* If you define string as character arrays they will not be constant.
	* When you declare an array, you declare a pointer to an address of the first element of the array.
	* When you create a pointer variable, the machine will allocate 4 or 8 bytes of space to store it. But what if you create an array?
	  The computer will allocate space to store the array, but it won’t allocate any memory to store the array variable. The compiler simply plugs in
	  the address of the start of the array. Because of that:
		* If you use the & operator on an array variable, the result equals the array variable itself.
		* Array variables don’t have allocated storage, it means you can’t point them at anything else.
	* Pointer Decay: If you assign an array to a pointer variable, then the pointer variable will only contain the address of the array.
	  The pointer doesn’t know anything about the size of the array, so a little information has been lost.
	* if `a = {6,7,8};` Then:
		* a[index]=index[a]. So a[2]=2[a]=8
		* a[0]=*a=6
		* a[1]=*(a+1)=7

## Strings
### Array of arrays:
``
char words [][10] = {
	"Hello",
	"Bye",
	"Thanks"
};
``
This first set of brackets is for the array of all string. The compiler can tell that you have five strings, so you don’t need a number between these
brackets. The second set of brackets is used for the length each individual string.

You can find an element by: words [1] ->  Bye. And an individual character by: words [1][1] -> y.

### Array of pointers
Very useful if you want to quickly create a list of string literals.
``
char *names_for_dog[] = {"Bowser", "Bonza", "Snodgrass"};*
``
This is an array that stores pointers, there will be one pointer pointing at each string literal. You can access the array of pointers just like you
accessed the array of arrays.

### Interesting functions
Functions inside the string.h library:
	* strstr(): Find the location of a string inside another string.
	* strchr(): Find the location of a character inside a string.
	* strcmp(): Compare two strings.
	* strcpy(): Copy one string to another.
	* strlen(): Find the length of a string.
	* strcat(): Concatenate two strings.
	* strdup(): function works out how long the string is, and then calls the malloc() function to allocate the correct number of characters on the heap.
	  you need to release the memory with free().

# Break and Continue
	* Break will allow you space loops at some point without the need to fail in the looping condition. Breaks don't break out on if's.
	* Continue skips the rest of the loop body and goes back the start of the loop.

# Return Values
	* Almost everything has a return value. For example: x = 4; will assign 4 to x and then return 4, so you could do y=x=4;
	* Return Codes:
		* EXIT_SUCCESS and EXIT_FAILURE for normal C programs.
		* Standard kernel convention is negative numbers for errors, 0 for success, positive numbers for "status" information.

# Booleans
	* True will always be any number greater than 0.
	* False will always be 0.
	* If you check that a function has been successful and it returns 0 when there are no errors you will have to do:
	  if(!(function))

# Memory Management
	* A variable declared inside a function is usually stored in the stack
	* A variable declared outside a function is stored in globals.

# Functions
In C, function names are pointer variables. The name of a function is a way of referring to a piece of code.s
	* If you declare an array argument to a function, it will be treated as a pointer.

## Function Pointers
Say you want to create a pointer variable that can store the address of each of the functions on the previous page.
You’d have to do it like this:

Function pointer structure:
```
[Return type](*[PointerVariable])([Param types])

For example:
char** (*names_fn)(char*, int)
```
PointerVariable is the name of the function you're declaring.
```
int (*warp_fn)(int);
warp_fn = go_to_warp_speed;		--> This will create a variable called warp_fn that can store the speed() address of the go_to_warp_ function.
warp_fn(4);						--> This is just like calling go_to_warp_speed(4).
char** (*names_fn)(char*,int);
names_fn = album_names;			--> This will create a variable called names_fn that can store the address of the album_names() function.
char** results = names_fn("Sacha Distel", 1972);
```
```
int sports_no_bieber(char *s)
{
return strstr(s, "sports") && !strstr(s, "bieber");
}

void find(int (*match)(char*) --> this is how you declare a function that takes a function pointer.
{
	...
}
```
For more information read page 320-321 of Head First C Book

## Function pointer arrays
Contains a set of function names that if combined with enum can help to safe code by not adding switch statements.
When C creates an enum, it gives each of the symbols a number starting at 0, if when you create the function pointer array you respect the same order than in enum. The syntax is:
```
/*
 * [Return type](*[PointerVariable])([Param types])
 * for example:
 */

void dump (){
	...
};
void second_chance (){
	...
};
void marriage (){
	...
};

enum response_type {DUMP, SECOND_CHANCE, MARRIAGE};
typedef struct {
	char *name;
	enum response_type type;
} response;

void (*replies[])(response) = {dump, second_chance, marriage}; <-- The function pointer

/*
 * In this way "replies[SECOND_CHANCE] == second_chance" because SECOND_CHANCE is 1. So you can do:
 */
int main()
{
	response r[] = {
		{"Mike", DUMP}, {"Luis", SECOND_CHANCE},
		{"Matt", SECOND_CHANCE}, {"William", MARRIAGE}
};
int i;
 for ( i = 0; i < 4; i++){
	 (replies[r[i].type])(r[i]); <-- Here you execute all functions depending on the response without a switch.
 }
 return 0;
}
```

being:
	* replies: Your arrays of fucntion names
	* r[i].type: a value like 0 for DUMP or 2 for MARRIAGE
	* replies[r[i].type]: The whole thing is a function, like "dump" or "marriage".
	* r[i]: Calling of the function and passing it the response data r[i].

## Variadic Functions

```
#include <stdarg.h> --> All the code to handle variadic functions is in stdarg.h
void print_ints(int args, ...) --> an ellipsis after the argument of a function means there are more arguments to come.
{
	va_list ap; --> A va_list will be used to store the extra arguments that are passed to your function.
	va_start(ap, args); --> C needs to be told the name of the last fixed argument.
	int i;
	for (i = 0; i < args; i++) {
		printf("argument: %i\n", va_arg(ap, int)); -->Your arguments are all stored in the va_list, you can read
																									them with va_arg. va_arg takes two values: the va_list and
																									the type of the next argument. In your case, all of the arguments
																									are ints.
	}
	va_end(ap); --> you need to tell C that you’re finished. You do that with the va_end macro.
}
```
# Operators
	* The difference with functions is: An operator is compiled to a sequence of instructions by the compiler. But if the code calls a
	  function, it has to jump to a separate piece of code
	* sizeof(arg); returns the size or arg. If it is used to measure an array which is an argument from a function it will return the
	  size of the pointer. If sizeof is used directly with a regular array it will return the length of the array. Example:

	void foo (char lat[])
	{
        	printf("size of lat = %i\n", sizeof(lat));
	}

	void main()
	{
        	char lat[] = "Hello World";

        	printf("size=%i bytes\n", sizeof(lat));

        	foo("Hello World");
        	foo(lat);
	}

	The code above will output:

	size=12 bytes	--> Lenght of the array.
	size of lat = 8	--> Size of the pointer.
	size of lat = 8	--> Size of the pointer.

# Creating command-line arguments
The main() function can read the command-line arguments as an array of strings. Actually, because C doesn’t really have strings built-in,
it reads them as an array of character pointers to strings.

```
int main(int argc, char *argv[])
{
}
```

**argc** is the number of elements in the argv array.
**argv** is the array of arguments.

```
"./program" "foo" "doo" "too"
```

program would be argv[0], foo argv[1], doo argv[2] and too argv[3].

**getopt()** it's a funtion from the **unistd.h** library which each time you call it, it
returns the next option it finds on the command line. You need to call it in a look like this:

> rocket_to -e 4 -a Brasilia Tokyo London

```
while ((ch = getopt(argc, argv, "ae:")) != EOF) --> The '"ae:"' means  'a' and 'e' are valid options. ":" means that the option needs and extra argument,
  switch(ch) {					    getopt() will point to that argument with the optarg variable.
  ...
  case 'e': --> You are reading the argument for the "e" option here.
    engine_count = optarg;
  ...
}

/*
 * These final two lines make sure we skip past the options we read
 */
argc -= optind;  --> stores the number of strings read from the command line to get the past options.
argv ++ optind;
```

Inside the loop, you have a switch statement to handle each of the valid options. When the loop finishes, you tweak the argv and argc variables
to skip past all of the options and get to the main command-line arguments.

If you want to add a "-" after in an argument, in order to avoid ambiguety you need to put "--". -t -- -4

# Data types
Storing a small data type in a large data type it's all right, but never do it in the other way around.

```1
short x = 15;
int y = x;
printf("The value of y = %i\n", y); --> This will say that y = 15.
```
```
int x = 100000;
short y = x;
print("The value of y = %hi\n", y); --> The value of y = -31072, creating an error.  %hi is the proper code to format a short value.
```

To discover the data size for your platform, use the limits.h and  float.h headers.

> printf("The value of INT_MAX is %i\n", INT_MAX);

## Casting
Converts values to other data types on the fly. If the compiler sees you are adding, subtracting, multiplying, or dividing a floating-point value with
a whole number, it will automatically cast the numbers for you.
int x = 7;
int y =2;

float z = (float)x/y; --> z = 3.5;

# Functions order

* Functions must be declared in order of dependency between them, first the most independet and lastly the one that depends on the rest of them.
* If this is not done, when the compiler arrives to the function call, it will guess that the return value will be an int, and if it is not the program will break once the function reaches the function declaration.
* In situations in which there is not correct order (functions might be mutually recursive), split the declaration from the definition. Declare both functions on top of the source file (so the compiler knows about them and doesn't need to do assumptions) and later on define them.
* An alternative to the previous method is to create a header file.
	- Create a new file with ".h" extension.
	- Include your declarations on it. No need of a main() function.
	- Include the header in your program. Make sure to surround the header name with brackets. When the compiler sees an include line with angle brackets (<>), it assumes it will find the header file somewhere off in the directories where the library code lives. But your header file is in the same directory as your .c file. By wrapping the header filename in quotes (""), you are telling the compiler to look for a local file.

# Sharing code
* Headers with "<>" on a Unix-style system like the Mac or a Linux machine, the compiler will look for the files under the standard directories:
> /usr/local/include
> /usr/include

If you want to share code between different projects/programs, you need to share headers and/or object files.

## Sharing header files
- Store them in a standard directory.
- Put your full pathname in your include statement: #include
> "/my_header_files/encrypt.h"
- Tell the compiler where to find them with the -I option on gcc:
> gcc -I /my_header_files test_code.c ... -o test_code

## Share object files
- Using full pathname
> gcc -I/my_header_files test_code.c /my_object_files/encrypt.o /my_object_files/checksum.o -o test_code

### Using archives of object files
The nm command lists the names that are stored inside the archive.
#### Create an archive with the ar command...
The archive command (ar) will store a set of object files in an archive file:
```
ar -rcs libhfsecurity.a encrypt.o checksum.o
r: the .a file will be updated if it already exists
c: the archive will be created without any feedback.
s: create an index at the start of the .a file
```
Make sure you always name your archives lib<something>.a. If you don’t name them this way, your compiler will
have problems tracking them down. The names begin with lib because they are static libraries.

#### ...then store the .a in a library directory
Options are:
* You can put your .a file in a standard directory like /usr/local/lib.
* Put the .a file in some other directory or create your own library directory.

#### Finally, compile your other programs
* If you’ve installed your archive in a standard directory, you can compile your code using the -l switch:
> gcc test_code.c -lhfsecurity -o test_code

* If you put the archive somewhere else you will need to use the -L option to say which directories to search:
> gcc test_code.c -L/my_lib -lhfsecurity -o test_code

# Link programs
## Static linking

To link a set of source files and generate a single program:

`gcc program1.c program2.c -o final_program`

If you want to change a single file and not have to recompile all the source files, you have to save the object code from each of them in separate files.

`gcc -c *.c`

-c will tell your compiler that you want to create an object file for each source, but you don't want to link them together.

Now you can link all the object files doing:

`gcc *.o -o final_program`

So if you just changed a single file and you don't want to compile all of them you can do:

`gcc -c program1.c`  -> this will recreate program1.o

`gcc *.o -o final_program`

You can’t change the different pieces of object code in an executable file.

## Dynamic Linking
The program is made up of lots of separate files that only joined together when the program runs. There are some things
our dynamic library files will need, like the names of the other files they need to link to.

A dynamic library can be built from several .o object files, which must be properly linked together in a dynamic library to form a single piece of object code.

### First create an object file

```
gcc -I/includes -fPIC -c hfcal.c -o hfcal.o
```
-fPIC tells gcc that you want to create position-independent code so that OS and processors can decide at
runtime where they want to load it into memory.

### Second convert the object file into a dynamic Library
What you call dynamic libraries depend on the platform:
* On Windows they're called Dynamic Link Libraries (.dll)
* On Linux they're called shared object files (.so)
* On Mac they're called dynamic libraries (.dylib)
```
gcc -shared hfcal.o -o /libs/libhfcal.so
```

The `-shared` option tells gcc that you want to convert a .o object file into a dynamic library. When the compiler creates the dynamic library, it will store the name of the library inside the file, so if you compile a library with one name, you can’t just rename the file afterwards.

### Compiling the elliptical program
Once you’ve created the dynamic library, you can use it just like a static library.
```
gcc -I\include -c elliptical.c -o elliptical.o
gcc elliptical.o -L\libs -lhfcal -o elliptical
```
The compiler will insert some placeholder code that will track down the library and link to it at runtime.

### Running the program
On Linux, the compiler just records the filename of the libhfcal.so library, without including the path name. That means if the library is stored outside the standard library directories, the program won’t have any way of finding the hfcal library. To get around this, Linux checks additional directories that are stored in the `LD_LIBRARY_PATH` variable. If you make sure your library directory is added to the LD_LIBRARY_PATH—and if you make sure you export it—then elliptical will find libhfcal.so.
```
> export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/libs
> ./elliptical
```
# Make

make is a tool that can run the compile command for you. The make tool will check the timestamps of the source files and the generated files,
and then it will only recompile the files if things have gotten out of date.

Every file that make compiles is called a target. A target is any file that is generated from some other files.
For every target, make needs to be told two things:
* The dependencies: Which files the target is going to be generated from.
* The recipe. The set of instructions it needs to run to generate the file.
Together, the dependencies and the recipe form a rule. A rule tells make all it needs to know to create the target file.

So if you are compiling  program1.c into program1.o:
* program1.o is the **target**.
* program1.c is the **dependency**.

Once you build the program1.o you are going to use it to build final_program. That means that final_program can be set as a target too.
The dependency to it are all the object files created in the previous steps.

All of the details about the targets, dependencies, and recipes need to be stored in a file called Makefile. Makefile example:

```
program1.o: program1.c headers.h
	gcc -c program1.c

program2.o program2.c headers.h
	gcc -c program2.c

final_program: program1.o program2.o
	gcc program1.o program2.o -o final_program
```

# Structures

Sets of data bundled together, lets you take different pieces of data into the code and wrap them up into one large new data type. This will create
a new custom data type that is made up of a collection of other pieces of data. It’s fixed length and the pieces of data inside the struct are given names.

Example:

```
struct fish {
	const char *name;
	const char *species;
	int teeth;
	int age;
};

struct fish snappy = {"Snappy", "Piranha", 69, 4};
```
Struct fields are accessed by name:
```
printf("Name %s\n", snappy.name);
```
You can also create structs from other structs. It makes easier to  cope with complexity. By combining structures together, you can create larger and larger data structures.
Once you’ve combined structs together, you can access the fields using a chain of “.” operators:

```
struct preferences
	const char *food;
	float exercise_hours;
};

struct fish {
	const char *name;
	const char *species;
	int teeth;
	int age;
	struct preferences care;
};

printf("Snappy likes to eat %s", snappy.care.food);
```

You can initialize structures like:
```
struct fish gol{.name = "Dory"};
```
When working with pointers, to access an element of a struct use -> instead of .
```
printf("Snappy likes to eat %s", snappy->care->food);
```

## typedef

Creates an alias for an struct that you created.
```
typedef struct cell_phone {
	int cell_no;
	const char *wallpaper;
} phone;

phone p = {5585, "sinatra.png"};
```
# Unions

A union will use the space for just one of the fields in its definition. The computer will give the union enough space for its largest field, and then
leave it up to you which value you will store in there. There will only ever be one piece of data stored. The union just gives you a way of creating a variable that
supports several different data types.

# Enum

enum lets you create a list of symbols.
```
enum colors {RED, GREEN, PUCE};
```

so you can do:

```
if (colors == RED)
```

and access only the element RED of the Union, avoiding segfault.

# Bitfields

A bitfield lets you specify how many bits an individual field will store.

```
typedef struct {
	unsigned int low_pass_vcf:1;
	unsigned int filter_coupler:1;
	unsigned int reverb:1;
	unsigned int sequential:1;
} synth;
```

Each field should be an unsigned int.
The ```:1``` means the field will only use 1 bit of storage.

If you have a sequence of bitfields, the computer can squash them together to save space. So if you have eight single-bit bitfields, the computer can store them in a
single byte. But if the compiler finds a single bitfield on its own, it might still have to pad it out to the size of a word.

# Linked list

Linked lists are like chains of data. A linked list is an example of an abstract data structure. It’s called an abstract data structure because a linked list is
general: it can be used to store a lot of different kinds of data. So linked lists allow you to store a variable amount of data, and they make it simple to add more data.

## Recursive structures

A struct that contains a link to another struct of the same type. To store a link from one struct to the next it's used a pointer. That way, the structure  will
contain the address of the next structure.
```
typedef struct island {
	char *name;
	char *opens;
	char *closes;
	struct island *next;
} island;
```
# Dynamic Storage

The heap is the place where a program stores data that will need to be available longer term. It won’t automatically get cleared away,
so that means it’s the perfect place to store data structures like our linked list.

## 1st: Get memory with malloc()

You have to tell the malloc() function exactly how much memory you need, and it asks the operating system to set that much memory aside in the heap. The
malloc() function then returns a pointer to the new heap space. Most of the time, you probably don’t know exactly how much memory you need
in bytes, so malloc() is almost always used with an operator called sizeof.

## 2nd: Return the memory with free()

Once you’ve asked for space on the heap, it will never be available for anything else until you tell the C Standard Library that you’re finished with it.
There’s only so much heap memory available, so if your code keeps asking for more and more heap space, your program will quickly start to develop memory
leaks you need to release the memory using the free() function.

Example:
```
island *p = malloc(sizeof(island));

free(p);
```

# Valgrid

valgrind can monitor the pieces of data that are allocated space on the heap. When your program wants to allocate some heap memory, valgrind will intercept
your calls to malloc() and free() and run its own versions of those functions. The valgrind version of malloc() will take note of which piece of code is calling
it and which piece of memory it allocated.

You don’t need to do anything to your code before you run it through valgrind. But to really get the most out of valgrind, you need to make sure your executable
contains debug information. To add debug info into your executable, you need to recompile the source with the -g switch:
```
gcc -g spies.c -o species
```

options:
> --leak-check=full

If you run your program several times, you can narrow the search for the leak.

# Processes and system calls
## System Calls
C programs make system calls if they want to talk to the hardware. System calls are just functions that live inside the operating system’s kernel.

### Functions to interact with the system
#### system()
system() takes a single string parameter and executes it as if you had typed it on the command line, is an easy way of running other programs from your code. Problems:
* If the comments contain apostrophes, might break the quotes in the command.
* What if the PATH variable causes the system() function to call the wrong program?
* What if the program we’re calling needs to have a specific set of environment variables set up first?

#### exec()
The exec() functions replace the current process by running some other program. You can say which command-line arguments or environment variables to use, and when the
new program starts it will have exactly the same PID as the old one. Ther are two groups of exec() functions: the list functions and the array functions.
##### The list functions: execl(), execlp(), execle()
The list functions accept command-line arguments as a list of parameters:
* The program. This might be the full pathname of the program —execl()/execle()—or just a command name to search for—execlp()— but the first parameter tells the exec() function what program it will run.
* The command-line arguments. You need to list one by one the command-line arguments you want to use. Remember: the first command-line argument is always the
name of the program. That means the first two parameters passed to a list version of exec() should always be the same string.
* NULL. After the last command-line argument, you need a NULL. This tells the function that there are no more arguments.
* Environment variables (maybe). If you use execle() you can also pass an array of environment variables. This is just an array of strings like "POWER=4", "SPEED=17", "PORT=OPEN", ....

If you want to run a program using command-line arguments and environment variables, you can do it like this:
```
char *my_env[] = {"JUICE=peach and apple", NULL}; --> Create a set of environment variables as an array of string pointers
execle("diner_info", "diner_info", "4", NULL, my_env); --> exacle passes a list of arguments and an environment.
```
##### The array Functions: execv(), execvp(), execve()
If you already have your command-line arguments stored in an array, you might find these two versions easier to use:

```
execv("/home/flynn/clu", my_args); --> execV = an array or VECTOR of arguments.
execvp("clu", my_args); --> execVP = an array/VECTOR of arguments + search on the PATH.
The arguments need to be stored in the my_args string array.
```
The only difference between these two functions is that execvp will search for the program using the PATH variable.

#### getenv()
getenv can read environment variables.

#### fork()
fork() makes a complete copy of the current process. The brand-new copy will be running the same program, on the same line number. It will have exactly the same variables that contain exactly the same values. The only difference is that the copy process will have a different process identifier from the original.
The original process is called the parent process, and the newly created copy is called the child process.

You call fork() like this:
> pid_t pid = fork();

fork() will actually return an integer value that is 0 for the child process and positive for the parent process. The parent process will receive the process identifier of the child process. But what is pid_t? Different operating systems use different kinds of integers to store process IDs: some might use shorts and some might use ints. So pid_t is always set to the type that the operating system uses.

##### Running a child process with fork() and exec()
he exec() functions replace the current function by running a new program terminating the original program immediately. To avoid that you need to call exec()
on a child process, that way, the parent process will be able to continue running.
* Make a copy of your current process by calling fork().
* if you are the child process, call exec()
### errno
The errno variable is a global variable that’s defined in errno.h, that tells you why a system call failed.
`strerror()` strerror() converts an error number into a message.

# Interprocess communication
Every process needs somewhere to record where data streams like the Standard Output are connected. Each data stream is represented by a file descriptor, which, under the surface, is just a number. The process keeps everything straight by storing the file descriptors and their data streams in a descriptor table.

The descriptor table has one column for each of the file descriptor numbers. Even though these are called file descriptors, they might not be connected to an actual file on the hard disk. Against every file descriptor, the table records the associated data stream. That data stream might be a connection to the keyboard
or screen, a file pointer, or a connection to the network.

The first three slots in the table are always the same. Slot 0 is the Standard Input, slot 1 is the Standard Output, and slot 2 is the Standard Error. The other slots in the table are either empty or connected to data streams that the process has opened. When the process is created, the Standard Input is connected to the keyboard, and the Standard Output and Error are connected to the screen. And they will stay connected that way until something redirects them somewhere else. if I open a new file, it is automatically added to the descriptor table.

## Proccess can redirect themselves
Processes can do their own redirection by rewiring the descriptor table.

Every time you open a file, the operating system registers a new item in the descriptor table
### fileno()
fileno() doesn’t return –1 if it fails. As long as you pass fileno() the pointer to an open file, it should always return the descriptor number.

### dup2()
dup2() duplicates a data stream from one slot to another.

# waitpid()
Once that child process gets created, it’s independent of its parent. how could you wait for it?
The waitpid() function won’t return until the child process dies.
```
wait (pid, pid status, options)
```
When the waitpid() function has finished waiting, it stores a value in pid_status that tells you how the process did. To find the exit status of the child process, you’ll have to pass the pid_status value through a macro called WEXITSTATUS():
```
if (WEXITSTATUS(pid_status))
puts("Error status non-zero");
```

## Listening to child Processes
* Connect your processes with pipes (and grep?)
Whenever you pipe commands together on the command line, you are actually connecting them together as parent and child processes.

### pipe()
pipe creates two connected streams and adds them to the table. When pipe() creates the two lines in the descriptor table, it will store their file descriptors in a
two-element array:
```
int fd[2];
if (pipe(fd) == -1) {
error("Can't create the pipe");
```
The pipe() command creates a pipe and tells you two descriptors: fd[1] is the descriptor that writes to the pipe, and fd[0] is the descriptor that reads from the
pipe. Once you’ve got the descriptors, you’ll need to use them in the parent and child processes.

#### In the child
you need to close the fd[0] end of the pipe and then change the child process’s Standard Output to point to the same stream as descriptor fd[1].
```
close(fd[0]); --> This will close the end of the pipe
dup2(fd[1], 1); --> This child connect the write end to the Standard Output
```
That means that everything the child sends to the Standard Output will be written to the pipe.

#### In the parent
you need to close the fd[1] end of the pipe (because you won’t be writing to it) and then redirect the parent process’s Standard Input to read its data
from the same place as descriptor fd[0]:
```
dup2(fd[0], 0); --> The parent connects the read end to the standard output
close(fd[1]);   --> This will close the write end of the pipe
```
Everything that the child writes to the pipe will be read through the Standard Input of the parent process.

## The death of a process
When you call the fgets() function, the operating system reads the data from the keyboard, and when it sees the user hit Ctrl-C,
sends an interrupt signal to the program.
A signal is just a short message: a single integer value. When the signal arrives, the process has to stop whatever it’s doing and go deal with the signal. The process looks at a table of signal mappings that link each signal with a function called the signal handler. The default signal handler for the interrupt
signal just calls the exit() function.

## Catching signals and running your own code
Sometimes you’ll want to run your own code if someone interrupts your program.

### sigaction
A sigaction is a struct that contains a pointer to a function. sigactions are used to tell the operating system which function
it should call when a signal is sent to a process. So, if you have a function called diediedie() that you want the operating system
to call if someone sends an interrupt signal to your process, you’ll need to wrap the diediedie() function up as a sigaction.
```
struct sigaction action; --> Create a new action
action.sa_handler = diediedie; --> This is the name of the function you want the computer to call.
sigemptyset(&action.sa_mask); --> The mask is a way of filtering the signals that the sigaction will handle, you usually want to use an empty mask.
action.sa_flags = 0; --> some additional flags, you can just set them to 0.
```
The function wrapped by a sigaction is called the handler because it will be used to deal with (or handle) a signal that’s sent to it.

#### All handlers take signal arguments
Signals are just integer values, and if you create a custom handler function, it will need to accept an int argument.
```
void diediedie(int sig)
{
	puts ("Goodbye cruel world....\n");
	exit(1);
}
```
Because the handler is passed the number of the signal, you can reuse the same handler for several signals. Handlers are intended to be short, fast pieces of code.
They should do just enough to deal with the signal that’s been received.

#### register a sigaction
Once you’ve create a sigaction, you’ll need to tell the operating system about it. You do that with the sigaction() function:
```
sigaction(signal_no, &new_action, &old_action);
```
* signal_no: The integer value of the signal you want to handle. Usually, you’ll pass one of the standard signal symbols, like SIGINT or SIGQUIT.
* &new_action: This is the address of the new sigaction you want to register.
* &old_action: If you pass a pointer to another sigaction, it will be filled with details of the current handler that you’re about to replace. If you don’t care
  about the existing signal handler, you can set this to NULL.

The sigaction() function will return –1 if it fails and will also set the errno function.
```
int catch_signal(int sig, void (*handler)(int))
{
struct sigaction action   --> Create an action.
action.sa_handler = handler;   --> Set the action’s handler to the handler function that was passed in.
sigemptyset(&action.sa_mask);   --> Use an empty mask
action.sa_flags = 0;
return sigaction (sig, &action, NULL);  --> Return the value of sigaction, so you can check for errors.
}
```
This function will allow you to set a signal handler by calling catch_signal() with a signal number and function name:
```catch_signal(SIGINT, diedieie)
```

# Avoiding duplicated error reporting code
Create function:
 ```
 void error(char *msg)
{
fprintf(stderr, "%s: %s\n", msg, strerror(errno));
exit(1);
}
```
call it like:
```
pid_t pid = fork();
if (pid == -1) {
error("Can't fork process");
}
if (execle(...) == -1) {
error("Can't run script");
}
```
