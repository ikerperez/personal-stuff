# Build software from source

If you want to build software from source but you don't want to mess up your laptop with bloodt deendencies do the next:
* Create a Dockerfile
* Install in the Dockerfile all the needed dependencies
* Write down all the commands needed to make the software
* clone the software repository
* Build docker image
* Run docker image bind mounting the software source repository into the working directory in the docker container.

Example to build rr debugger:

      ##############################################
      ##########################
      #      DOCKERFILE        #
      ##########################
      FROM  ubuntu:latest                                                              
      WORKDIR /rr-docker                                                               
                                                                                      
      RUN apt-get update && apt-get install -y \                                       
             ccache \                                                                 
             cmake \                                                                  
             make \                                                                   
             g++-multilib \                                                           
             gdb \                                                                    
             pkg-config \                                                             
             coreutils \                                                              
             python3-pexpect \                                                        
             manpages-dev git \                                                       
             ninja-build \                                                            
             capnproto \                                                              
             libcapnp-dev                                                             
     CMD cd rr && cd obj && cmake ../rr && make
     ###################################################

`$ docker build -t rr-builder .`

`$ docker run -it --mount=type=bind,src=$PWD,dst=/rr-docker rr-builder`


#RUN DOCKER INTERACTIVELY → $ docker run -it <image> /bin/bash
* See logs:
   * docker ps → to list images
   * docker -f logs image
