# 32 bit vs 64 bit machines
On 32-bit operating systems, a memory address is stored as a 32-bit number. A 64-bit uses 64 bit

# Memory
Next are described the memory "components" in order top-down.

## Stack
This is the section of memory used for local variable storageEvery time you call a function, all of
the function’s local variables get created on the stackIt starts at the top of memory and grows downward.

## Heap
The heap is for dynamic memory: pieces of data that get created when the program is running and then hang
around a long time.

## Globals
A global variable is a variable that lives outside all of the functions and is visible to all of them.
Globals get created when the program first runs, and you can update them freely

## Constants
Constants are created when the program first runs, but they are stored in read-only memory.

## Code
The code segment is also read-only. This is the part of the memory where the actual assembled code gets loaded.

