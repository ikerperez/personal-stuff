# VARIABLES and STRUCTURES
  * Reflect it use in the variable name.
  * The initialization should be after the last structure declaration.
  * The fields in structures will have to be documented.
  * Reverse Christmas tree order (longer first) is preferred for variable declarations.
  * Don't overload a variable. Reusing a variable is bad coding style.

# CONDITIONALS
  * Use  switches rather than multiple ifs. Use fallthough: Not setting a break so the next label executes the previous content.

# FIXUPS
  * you don't want to send fixes for previous patches of a patch series in that same series.

# Functions
  * Adding arguments to functions is costly, consider when they should be included or called from a pointer.
  * Parameter order should be more generic first.

# GENERAL STUFF
  * Each patch in the series has to be compilable at its place in the series.
  * The subject of a patch needs to reflect what it does.
  * No spaces before comments, try to align all comments.
