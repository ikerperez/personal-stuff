# Config file
The config file is loaded by default from `~/.kube/config`. However, it is
possible to load different ones by adding to the `kubectl` command the
`--kubeconfig` flag or setting up the `KUBECONFIG` to your config file.

# Commands
* **kubectl version**
* **kubectl get componentstatuses**: Diagnostic from the cluster.
  - *Controller-manager*: responsible for running various controllers that
    regulate behaviour in the cluster.
  - *cheduler* responsible for placing different pods onto different nodes in
    the cluster.
  - *etcd*: storage for the cluster where all the API objects are stored.
* **kubectl get nodes**: list all the nodes in your cluster
* **kubectl describe nodes ${NODE-NAME}**: Get information about specific node.

# Namespaces
kubernetes uses namespaces to organize objects in the cluster. Think of it as a
folder that holds a set of objects. *kubectl* interacts with the *default*
namespace, if you want to use a different one pass the *--namespace* flag.

Get namespaces:
```
kubectl get namespaces
```

Once you have created a namespace description file you can actually create the
namespace with:

```
kubectl create -f namespacename
```

## Contexts
To change default namespace permanently use contexts, that get record in
$HOME/.kube/config.
You can create a contexts with:

```
kubectl config set-context my-context --namespace=mystuff
```

And then start using it with:

```
kubectl config use-context my-context
```

# Working with objects
## Viewing them
* `kubectl get <resource_name>` will list all resources in the current cluster
  * `kubectl get <resource_name> <obj-name>` specific resource info.
  * `-o` gives you extended info. To view complete object do `-o json` or `-o yaml`.
* `kubectl describe <resource_name> <obj-name> Detailed information about a particular object.
## Creating, Updating and destroying objects
Once you have an object described in a file you can create it with:

```
kubectl apply -f obj.yaml
```
After you make changes to the object to can use the same command to update it.
To make interactive edits, instead of editing a local file use:

```
kubectl edit <resource_name> <obj_name>
```

The apply command records the history of previous configurations in an annotation
within the object. You can manipulate them with `edit-last-applied`,
`set-last-applied`, and `view-last-applied`. For example:

```
kubectl apply -f myobj.yaml view-last-applied

To delete an object:

```
kubectl delete -f obj.yaml

or

kubectl delete <resource-name> <obj-name>
```

# Pods
Represents a collection of application containers and volumes running in the
same execution environment. Applications running in the same Pod:
* share the same Ip address and port space.
* Have the same hostname.
* can communicate using native interprocess communication channels ober System V
  IPC or POSIX message queues.
Applications in different Pods are isolated from each other.

whn designing pods ask yourself: `Will these containers work correctly if the
land on different machines?` If the answer is `no` use a pod for grouping, if
use individual pods.

When you delete a pod all the data contained on it will be deleted, if you want
to persist data across multiple instances of a pod you need to use **PersistentVolumes**.

## Accessing your Pods
### Port Forwarding
Access a specific pod
```
kubectl port-forward pod 8080:8080
```

## Getting logs

```
kubectl logs pod
   -f: Continous stream of logs
   --previous: get logs froma previous instance of the container.
```

## Run commands in the container

```
kubectl exec node date
```

Interactive version:

```
kubectl exec -it node ash
```
## Copying files to and from containers
kubectl cp <pod_name>:/path/file.txt ./file.txt

## Health Checks
A health chek ensures that the main process of you application keeps running.

### Liveness probe
Liveness heatk chek run application-specific logic to verify that the application
is not just still running, but is functioning properly. Liveness probess are
defined per container, each container inside a pod is health-cheked separately.

### Readliness Probe
Readlines describes when a container is ready to serve user requests.

### tcpSocket health checks
Open a TCP socket

### exec probes
Execute a custom script or program in the comtexts of a container.

## Resource Limitation
### Minimum required resources
### Resource usage limits

## Persistent data with Volumes
Two more things to add to out pod configuration:
* *spec.volumes* section: This array defines all of the volumes that may be
   accessed by the containers in the Pod manifest.
* *volumeMounts: Defines the volumes that are mounter into a particular container,
  and the path where each volume should be mounted. Two different containers in
  a Pod can mount the same volume in different mount paths.

## Persistent data using remote disks
You can mount a remote network storage volume into your Pod. Whn using network-
based storage, Kubernetes automatically mounts and unmounts the appropiate
storage whenever a Pod using that volume is scheduled onto a particular machine.
Kubernetes includes support for standards protocols and cloud provider-based sotra APIs

Example using NFS:

```
# Rest of the Pod definition above here
volumes:
    - name "kuard-data"
      nfs:
        server: my.nfs.server.local
        path: "/exports"
