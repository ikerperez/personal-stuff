# Debugging

* The /dev/kmsg character device node provides userspace access to the kernel's printk buffer.
* printk is a funcition that prints messages and is used in the C Programming Language exclusively for the Linux Kernel. It accepts a string parameter called the format string, which specifies a method for rendering an arbitrary number of varied data type parameter(s) into a string. In kernel mode, you can't use the standard C library, so printf is not available, hence the need for printk. (pr_debug")

* Tools:
   * ftrace
   * strace
   * LLT → https://lttng.org/
   * rr

# Mount

* The mount command itself can only mount devices into directories, for example: “mount /dev/sda / ~/USB1”
* If you want to mount a directory into another one, or a file into another one you need to use the --bind option, for examaple: “mount --bind ~/Images ~/Music” this would mount the Images directory into the Music directory, so if you “ls” in Music you will see the content of Images.
* To mount directories as rw or ro use: mount -o rw,remount /directory

# File Read

* head lets you read multiple files diffenced between them. It lets you select how many lines do you want to see too, for example “head -20 foo.txt bar.txt”
* hexdump -C and xxd: Read compiled files

# PATH

* pwd: To obtain the path of the current directory. pwd is the binary version of $PWD.
* realpath: To obtain the full path to a file/directory

# Special Characters

@ Expands  to  the  positional  parameters,  starting  from  one.  When the expansion occurs within double quotes, each parameter expands to a separate word.  That is, "$@" is equivalent to "$1" "$2". Takes all the input.

# Kernel stuff

* Modprobe: add and remove modules from Linux kernel
* modules.dep: defines all built the modules and it's dependencies.
* Module_parameter(Variable, type, permissions) : method to give command line arguments to a module

# Uncategorized

md5sum: calculates checksum from files, very useful to compare files
strings: extracts the cointained strings from binaries.

# ACPI tables
ACPI tables define the hardware devices that will be connected to a system. Each device will have a compatible string and that string will be present in the driver too. When the system boots up will look for drivers with the same compatible string to select the driver to be used.

## ACPI

• Instantiating Device Tree devices with ACPI (tmp75b example)

    /* tmp75b (lm75) temperature sensor */
    Scope (_SB.PCI0.I2C1)   Here is defined the address to the device
    {
        Device (TMP0)            Here I give a name to the device
        {
            Name (_HID, "PRP0001")        This is a way to switching up the device, however here a special string is used “PRP0001” because it tells acpi that the match with the driver should be done using  .of_match_table instead.  Which means that can match with devicetree devices.
            Method (_CRS, 0, Serialized)  // _CRS: Current Resource Settings
            {                                                                                                         Here we are defining the characteristics of the device, in this case we are specifing its I2C port parameters
                Name (SBUF, ResourceTemplate ()                                                    For that porpoise, it is used “Method (_CRS, ” , inside it the specics can be defined and the a buffer must be
                {                                                                                                     returned, so the kernel can obtaion all that information.
                    I2cSerialBusV2 (0x48, ControllerInitiated, 400000,
                        AddressingMode7Bit, "\\_SB.PCI0.I2C1",
                        0x00, ResourceConsumer, , Exclusive,
                        )
                })
                Return (SBUF) /* \_SB_.PCI0.I2C1.TMP0._CRS.SBUF */
            }
            Name (_DSD, Package() {                                                                    _DSD with the ToUUID("d is a way of defining a lis t of properties of a hardware. That together with  Name (_HID, "PRP0001")
                ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),                      allows us to match a string “ti,tmp75b” in this case, with the same in the compatible structure of the device driver.
                Package () {
                    Package (2) { "compatible", "ti,tmp75b" },
                }
            })
        }
    }

## NHLT

NHLT is an audio configuring “system”. It will be composed by 2 elements:
• The Audio topology: Here are described all the devices connected to the DSP, Front End ones and Back End ones.
• The NHLT Table: The NHLT table will have described on it each audio device's endpoint, it's comfiguration and the path tp it's blob.

The Blobs are binary files that they describe the comfiguration of the device.

The NHLt table can mi compiled using an intel's tool called nhltc.
For the topology there is an Intel's tooling.

# Intel stuff

## Graphics
### i915
* GuC needs to be enabled by adding `enable_guc_loading=2` and `enable_guc_submission=1` to i915 module configuration. See drivers/gpu/drm/i915/i915_params.c. Commit: 041824ee25cfc535ba2d9a22c217df735ea2471e

# Apparmor

This is a tool that the rights of the applications. Each application will have a profile in apparmor.d/ that will define to wich files can perform operations.

# Interpreting segfault

example: segfault at 10 ip 00007f9bebcca90d sp 00007fffb62705f0 error 4 in libQtWebKit.so.4.5.2[7f9beb83a000+f6f000]


    address (after the at) - the location in memory the code is trying to access (it's likely that 10 and 11 are offsets from a pointer we expect to be set to a valid value but which is instead pointing to 0)
    ip - instruction pointer, ie. where the code which is trying to do this lives
    sp - stack pointer

    error - An error code for page faults; see below for what this means on x86.

    /*
     * Page fault error code bits:
     *
     *   bit 0 ==    0: no page found       1: protection fault
     *   bit 1 ==    0: read access         1: write access
     *   bit 2 ==    0: kernel-mode access  1: user-mode access
     *   bit 3 ==                           1: use of reserved bit detected
     *   bit 4 ==                           1: fault was an instruction fetch
     */


# I2C

Instantiate a new device:
* echo tmp75b 0x48 > /sys/bus/i2c/devices/i2c-1/new-device 
* Documentation/i2c/instantiating-devices
