# First Rule: Understand the System

You need a working knowledge of what the system is supposed to do, how it's designed and why it was designed that way.

## READ THE MANUAL:
  It will tell you what you're supposed to do to it and how it's supposed to act as a result. Read functional specifications, 
  design specs, schematics, timing diagrams, state machines, source code, comments. But don't necessarily trust this 
  information, it can be wrong.
## READ EVERYTHING, COVER TO COVER: 
  Don't ignore anything, it may be the cause of the bug. Application notes, implementation guides, warnings about common 
  mistakes, latest documentation, reference designs.
## KNOW WHAT'S REASONABLE: 
  You have to know how the system would normally work. Know about the fundamentals of your technical field.
## KNOW THE ROAD MAP:
  Need to now the lay of the land, too divide a system in order to isolate the problem depend on knowing what functions are
  where. Understand what all the blocks, interfaces, APIs, communication interfaces, modules and programs do. Know how 
  "Black-Boxes" are supposed to do to locate if the problem is inside or outside the box.
## KNOW YOUR TOOLS:
  You have to be able to choose the right tool, use it and interpret the results correctly. Take your time to learn 
  everything you can about your tools. Know the limitations of the tools. You have to know about your development tools too,
  programming language, compiler, linker, how data is aligned, references are handled and memory is allocated.
## LOOK IT UP:
  Don't guess, look it up. Don't trust your memory.

# Second Rule: Make it Fail

Make the system fail again. Reasons:

* So you can look at it. 
* So you can focus in the cause. Knowing under exactly what conditions it will fail helps you focus on probable causes.
* So you can tell if you've fixed it.Once you think that you have fixed, reproduce the conditions for the fail to check
  if you have actually fixed it.

## Do it Again
A well-documented test procedure helps to repeat the fail, you need to make it fail multiple times. Look at what you did
and do it again. Write down each step as you go, then follow tour own written procedure to make sure it really causes the error.

## Start at the Beginning
Bugs can depend on a complex state of machine, you have to be careful to note the state of the machine going into your sequence.
Try to start the sequence from a know state, such as a freshly rebooted computer, then reproduce all the steps that were 
executed during last error (click an icon 5 times, switch on and of a program etc).

## Stimulate the Failure, don't simulate it
When the failure sequence requires a lot of manual steps it can be helpful to automate the process. Reproduce the same 
situation that caused the problem last time. **Simulate** the conditions **conditions** that **stimulate** the **failure**
For example: A dentist will spray cold air over your teeth to find the cold-sensitive spot. 

Avoid simulation the failure mechanism itself, your simulated system will work flawlessly all the time it fail in a new 
way that distracts you from the original bug. Don't change the mechanism that creates the failure. Simulating the bug
by trying to re-create in a similar system is more useful, within limits. If a bug can be re-created on more that one system, 
you can characterize it a as a design bug. If it only is reproduced in some configurations and not in others it may help you
narrow down the possible causes. But it you can't re-create it quickly, don't start modifying your simulation to get it happen.
You'll be creating new configurations, not looking to a copy of the one that failed. When you have a system that fails, go 
after the problem on *that* system in *that* conditions.

## What if it's intermittent?
You know exactly what you did, but you don't know exactly how you made it fail. Possible factors you didn't notice:
initial conditions, input data, timing, outside processes, electrical noise, temperature, vibration, network traffic.
Try to get control of all conditions. **First figure out the conditions**, in software look for uninitialized data, random
data input, timing variations, multithread synchronization and outside devices. In hardware look fro noise, vibration, 
temperature, timing an parts variation. **Once you know the conditions** try a lot of variations: initialize arrays with 
a know pattern, control the timing and the vary it, tweak clock speed inject noise. If controlling the conditions makes 
the problem go away try every possible value of that condition until you hit the cause of the problem. If you can't control 
the condition try to increase the likelihood of it by making it more random. **Watch out that the applified condition isn't
causing a new error**.

## What if it's still intermittent

### A hard look at bad luck
Look to the failure each time it fails, while ignoring the many times it doesn't fail. Capture information on every run so
you can look at it after you know that it's failed. Save as much system information (logs). Compare the logs. No the things
that happen only in the failure cases.

### Lies, Damn Lies, and statistics

When a problem is intermittent you may start to see patters that seem to be associated to the failure. Don't get carried away
by it. A lot of times, coincidences will make you think that one condition makes the problem more likely to happen than other
condition, and waste a lot of time chasing the difference between them. When you capture enough information  identify things
that are *always* associated or *nerver* associated with the bug.

## Did you fix it, or did you get lucky?
Use statistical testing to prove the bug was solved, run many-many samples. If the bug failed 1/10 times and now fails 1/30
but you gave up at try 20 you won't see it fail. Find  a sequence of events that always goes with the failure and when it
happens you get the 100% of failure. When you think you fixed the bug, run test until the sequence occurs, if there is no
failure, you fixed it.

## But that can't happen
"That" is the failure mechanism that the tester is assuming is behind the problem, or the sequence of events that seems to
be the key to reproduce the problem. But it is very possible that "That" is not what is happening. Forget about the
assumptions, accept the data and look further into the situation with open mind and parallel thinking.

## never throw away a Debugging tool
Use good engineering practices to create the tools (techniques, documentation, maintainable, upgradeable) to develop your
testing tools, it might be useful next time.

# Third Rule: Quit Thinking and Look

Seeing the low-level failure is crucial. If you guess at how something is failing, you often fix something that isn't the
bug. In hardware hook up a scope and a logic analyser, figure out triggers. In software put breakpoints, add debug
statement, monitor program values and examine memory.

## See the Failure

What we see when when we note the bug is the *result* of the failure, but not the actual failure. Look closely to see the
failure in enough detail to debug it. Many problems are easily misinterpreted if you can't see all the way to what's
actually happening. You end up fixing something that you've guesses is the problem, but it was something different that failed.

## See the details

You need to decide if it is worth to keep looking deeper into the failure to get more detail. Eventually, you get enough detail
that it makes sense to look to the design and figure out what the cause of failure is. Keep looking until the failure you can
see has a limited causes to examine.

## See the failure in low lever detail

It will help you prove if is actually fixed.

## Instrument the System

You have to put instrumentation into or onto the system. Into the system is best, during design, build in tools that will help
you see what is going on inside. You might as well build in the debugging tools as well.

## Design Instrumentation in

**Electronic hardware:** Add test points. The more signals you can bring out the chip, the better. Make all registers r/w. Add LEDs
and status displays to see what is going on.

**Software:** compile in debug mode so you can watch  your program run with a source
debugger. In release mode, put interesting variables into performance monitors so you can watch them during run time.
Implement a debug window and have your code spit out and save status messages. Have a way to switch selected messages or
types of messages on and off, so you can focus in the ones you need spewing messages to a debug window often changes timing
which can affect the bug or bring the system processor to its knees.

Status messages can be switched on/off at 3 different levels: compile time, start-up time and run time. Try switching at run
time, because you can debug at any time.

Status message format: Break them into fields. One column system time, the others could be: module or file outputting the error,
run time data: errors, commands, status expected versus actual values.

**Embedded systems:** add sort of output display. If the embedded processor is built-in into  another computer, use the main
processor display, add communication like shared memory location or messages between both systems. For timing and hardware bits
that you can toggle up/down when you get in/out routines.

Think about debugging from the beginning of the design.

## Build in Instrumentation Later

Make sure to start with the same design base that the bug was found in, same build, same revision. Once you get the
instrumentation in, make it fail again to prove that you did use the right base and that your instrumentation didn't
affect the problem. Once you've found the problem, take all the stuff out so it doesn't clutter up the resulting product.

Look for things that will either confirm what you expect or show you the unexpected behaviour that's causing the bug.
Look for variables, pointers, buffer levels, memory allocation, even timing relationships flags, return codes, commands
data, window messages, function calls and exits.

## Don't be afraid to dive in

You should be willing to rebuild the software in order to find the bug in the first place. Make a debug version so
you can see the source. Add new debug statements to look at the parameters you really need to see and then "#ifdef".

## Add instrumentation on

**Hardware:** meters, scopes, logic analysers, spectrum analysers, thermocouples. Your equipments needs to be fast and accurate
enough to measure the things you're looking for.
**Software:** Debugger, bus orientated analysers.

## The Heisenberg Uncertainty Principle

Your test instrumentation affects the system under test, this is unavoidable. You just have to keep it in mind so the effects
don't take you by surprise. Some instrumentation methods are less intrusive than others. After adding instrumentation to a system,
make it fail again to prove that the Heisenberg isn't biting you

## Guess only to focus the search

You have to confirm that your guess is correct by seeing the failure before you try to fix it. If it turns out that careful
instrumentation doesn't confirm a particular guess, then it's time to back up and guess again. One reason to guess is that some
problems are easier to fix or more likely to happen, go for those first.

# Fourth Rule: Divide and Conquer

Divide between godness and badness and move into badness.

## Narrow the Search

Succesive aproximation is useful tehnique to target something within a range of possibilities. You start at one end of the range,
the go halfway to the other and see if you are past or not. If you are past, you go to 1/4 and try again. If you are not pas it,
you go 3/4s and try again. it depends in 2 details: You have to know the range of the search and when you look at a point, you need
to know which side of the problem you are on.

## In the Ballpark

Assume the entire system is the range, if you make the range smaller and the bug is out of it, you won't find it.

## Which side are you on?

You have to know your search range and that at one end things are good and at the other bad. Call it upstream and downstream.
Downstream means:
* Hardware: Farther out in the signal or data flow.
* Software: Later in the code flow.

## Inject Easy-to-spot Patterns

A way to make a subtle effect more obvious is to use a really easy-to-recognize input or test pattern. You have to be careful
that you don't change the bug by setting up new conditions. If the bug is pattern dependant, putting in an artificial pattern
may hide the problem. "Make it Fail" before proceeding.

## Start with the bad

Don't start at the good end confirming things that are correct. Start at the bad end and work your way upstream.

## Fix the bugs you know about

When you figure out  one of several simultaneous problems, fix it right away, before look for the others.

## Fix the noise first

Certain kinds of bugs are likely to cause other bus, so you should look for and fix them first.
* Hardware: noisy signals cause intermittent problems. Glitches and ringing clocks noise on analogue signals, jittery timing and
  bad voltage levels need to be taken care of before you look to other problems.
* Software: Bad multithread synchronization, accidentally reentrant routines, uninitialized variable inject randomness.
Don't get carried away, if you only suspect that noise is the issue, weight the difficulty of fixing the problem against the
likelihood that is really affecting the problem.

# Fifth rule: Change one thing at a time

After implementing a change trying to fix something, if it doesn't work, turn the change back.

## Use a rifle, not a shotgun

Change only one thing at a time, if you change lots of them at the same time, you might solve the bug, but you won't know which
was the faulty one. If you *really saw* exactly what was failing you will need to fix only that one thing.

Isolate and control variables: In order to see the effect of one variable, try to control all the other variables that might affect
the outcome (leave them by default).

## Grab the Brass Bar with Both Hands

Don't change different parts of the system to see if they affect the problem. You are changing conditions instead of looking to the
problem, this can hide the first problem and cause more.

## Change one test at a time

Changing the test sequence or parameter could cause a problem happen more regularly. This helps you see the failure, but still only change
one thing at the time, if it doesn't work back it out.

## Compare with a good one

If you have a system that fails and one that does not check their differences.  Scope traces, debug output, status windows, logs etc.
Compare the logs line to line. Narrow down the differences between both the two traces, get the rest of the stuff out of there. Don't
use different machine, different software, different parameters, different user input, different days or different environments. Be
prepared to look into the whole log.

## What did you change since the last time it worked

Sometimes the difference between a working system and a broken one is that a design change was made. You have to figure out which version
first caused the problem, going back and testing successively older versions until the failure goes away. Then go forward to the next version,
verify that that the failure occurs and compare both versions.

# Sixth rule: Keep an Audit Trail

Note everything, even if it is very obvious.

## Write down what you did, in what order, and what happened

Keep and audit trail of the testing. You have to know which results came from each step.

## The devil is in the details

Annotate every single detail. Kind of system, sequence of events leading to the failure, actual failure. Capture logs, but describe which is the
error in the log, which is the good log, symptoms etc. Annotate any debug traces or logs that you have with any conditions and symptoms that aren't
actually in the log. Correlate symptoms with time stamps. Be specific and consistent in describing things. Not only annotate what happened, but how much too.

## Correlate

Correlate symptoms with other symptoms, debug information and machine's time and human time. "it made a loud noise for 4 seconds starting at 14:15:05"

## Audit trails for design are also good for testing

Version control (git) is good to discover bugs because it allows you go back to the last working version and check which was the code that made it fail.
Version control let's you record information from the used tools, they could be the source of the bug too.

## The shortest Pencil is Longer than the Longest Memory

Never trust your memory with details, you will forget them. Write everything down, preferably write it electronically, so you can make backups, attach it
to reports, send it easily etc. Write down, what you did, what happened, how did you fix it, events, effects, theories etc.

# Seventh rule: check the plug

General requirements get overlooked, make sure you check them.

## Question your assumptions

Never trust your assumptions, ask yourself the age-old stupid question "is it plugged in?". Check the power, the clock, the drivers, the new code being loaded etc.

## Don't start at square three

Check if start-up conditions are correct. Initialize, reset, delete, everything that needs it.

## Test the tool

Check the tools you are using behave as you expect. Check default settings, building environment, right libraries. Test the test tool: Before using an oscilloscope
check the probe against 5V, check the print statements work etc.

# Eighth rule: Get a fresh view

## Ask for Help

### A Breath of fresh insight

It is hard to see the big picture from the bottom of a rut. Someone who comes at the problem from a unbiased viewpoint can give us great insights and trigger new
approaches. Sometimes explaining the problem to someone else gives you a fresh view, and you solve the problem yourself.

### Ask an expert or to the voice of experience

There might be people who know a lot about the thing you are working or the suffered the same problem before, ask them.

## Where to get help

Insight associates, knowledge management systems, the vendor, forums, web etc.

## Don't be Proud

Don't be afraid to ask, it is not a sign of incompetence, it is a sign of true eagerness to get the bug fixed.
Don't assume that you are and idiot and the expert is god, he can make errors too. Don't assume a bug is your error.

## Report Symptoms, not Theories

When you describe the problem, keep one thing in mind: Report symptoms, not theories. If you go to somebody fresh and lay a theory on her, you drag her right down
into the same rut you are in. Describe what happened, what you have seen, conditions but don't talk about what you think is the cause of the problem.

## You don't have to be sure

Sometimes you will see data that smells fishy, looks wrong or it seems to be related to the problem, but you are not sure why. Still worth presenting.

# Ninth rule: If you didn't fix it, It ain't fixed

## Check that it's really fixed

Don't assume that the fix works, test it, try to make the system fail.

## Check that it's really your fix that fixed it

+When you think you've fixed a design, take the fix out, make sure it's broken again, put the fix back, make sire it's fixed. During debugging you might have changed
something that isn't part of the official fix, sometimes it actually fixes or hides the problem, and your fix didn't really work.

## It never just goes away by itself

If you didn't fix it, it isn't fixed. The bug might be hiding or be intermittent, discover it, don't assume that it got fixed by itself.

## Fix the Cause

If a hardware device fails, don't assume that it broke for no reason. If there's a condition that will cause any good part to fail, changing the part will only
buy you little time before the new part fails too. You have to find the real failure, some other condition might have caused the failure.

## Fix the process

If there is oil in the floor and you clean it, the problem is not gone. There is a leaky fitting and it'll leak more, so you tighten it, but still not fixed because
The machine vibrates too much and it will go loose again, the real problem is that the machine it's held down by 2 bolts instead of four. Change the design process
in order so the vibration is properly accounted in the design process.
